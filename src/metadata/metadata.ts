export class Metadata<T> {
  private readonly key: symbol = Symbol(this.name);

  constructor(private readonly name: string) {}

  get(target: any): Set<T> {
    return this.getStoredSet(target) || this.getNewSet(target);
  }

  add(target: any, value: T): Set<T> {
    return this.get(target).add(value);
  }

  search(target: any, fn: (item: T) => boolean): boolean {
    for (const value of this.get(target).values()) {
      if (fn(value) === true) {
        return true;
      }
    }

    return false;
  }

  private getStoredSet(target: any): Set<T> | undefined {
    const descriptor = Reflect.getOwnPropertyDescriptor(target, this.key);
    if (descriptor && typeof descriptor.value === 'function') {
      return descriptor.value();
    }
  }

  private getNewSet(target: any): Set<T> {
    const value = new Set();
    Reflect.defineProperty(target, this.key, {
      value: () => value
    });
    return value;
  }
}

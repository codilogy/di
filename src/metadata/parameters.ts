import { Metadata } from './metadata';
import { InjectionPointMetadata } from '../injection-metadata';

export const ParametersMetadata = new Metadata<InjectionPointMetadata<any>>('di:parameters');

import { Metadata } from './metadata';
import { InjectionPointMetadata } from '../injection-metadata';

export const PropertiesMetadata = new Metadata<InjectionPointMetadata<any>>('di:properties');

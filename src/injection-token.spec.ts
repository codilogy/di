import { InjectionToken } from './injection-token';
import { ok, strictEqual } from 'assert';

describe('InjectionToken', () => {
  class Car {}

  before(() => {
    InjectionToken.create(Car);
  });

  it('can exchange class for its InjectionToken', () => {
    ok(InjectionToken.resolve(Car) instanceof InjectionToken);
  });

  it('can exchange string for corresponding InjectionToken', () => {
    ok(InjectionToken.resolve('Car') instanceof InjectionToken);
  });

  it('should treat function and string equal returning same token', () => {
    strictEqual(InjectionToken.resolve('Car'), InjectionToken.resolve(Car));
  });
});

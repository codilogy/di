import { ok, strictEqual, throws } from 'assert';
import { Injector } from './injector';
import { Injectable } from './decorators/injectable';
import { InjectionToken } from './injection-token';
import { InjectionMetadata } from './injection-metadata';
import { InjectionType } from './injection-type';

describe('Injector', () => {
  class Car {}
  const serialNo = 1111;

  let injector: Injector;
  let carToken: InjectionToken<Car>;
  let serialNoToken: InjectionToken<number>;

  beforeEach(() => {
    injector = new Injector('test injector');
    carToken = injector.set({
      token: Car,
      factory: () => Car
    });
    serialNoToken = injector.set({
      type: InjectionType.Value,
      token: 'serialNo',
      factory: () => serialNo
    });
  });

  describe('tokens list', () => {
    it('should be able to use it with for...of', () => {
      let count = 0;
      for (const token of injector.tokens()) {
        ok(typeof injector.provide(token) !== 'undefined');
        count++;
      }

      strictEqual(count, 2);
    });
  });

  describe('class registration', () => {
    const value = class Engine {};

    it('should be possible with class itself as a token', () => {
      const token = injector.set({
        token: value,
        factory: () => value
      });

      ok(token instanceof InjectionToken);
    });

    it('should be possible with custom string as a token', () => {
      const token = injector.set({
        token: 'engine token',
        factory: () => value
      });

      ok(token instanceof InjectionToken);
    });

    it('should be possible with InjectionToken instance as a token', () => {
      const token = new InjectionToken('engine');
      const injectionToken = injector.set({
        token,
        factory: () => value
      });

      ok(injectionToken instanceof InjectionToken);
      strictEqual(injectionToken, token);
    });
  });

  describe('custom value registration', () => {
    it('should register a string', () => {
      const token = injector.set({
        type: InjectionType.Value,
        token: 'SerialNo',
        factory: () => 'XYZ123'
      });

      ok(token instanceof InjectionToken);
    });
  });

  describe('token exchange', () => {
    it('should return metadata in exchange for class token', () => {
      ok(injector.get(Car) instanceof InjectionMetadata);
      ok(injector.get(carToken) instanceof InjectionMetadata);
    });
    it('should return metadata in exchange for primitive data token', () => {
      ok(injector.get(serialNoToken) instanceof InjectionMetadata);
    });
  });

  describe('providing value', () => {
    it('should resolve token to class', () => {
      ok(injector.provide(Car) instanceof Car);
    });
    it('should always return single instance of the class', () => {
      const a = injector.provide(Car);
      const b = injector.provide(Car);

      strictEqual(a, b);
    });
    it('should resolve token to primitive', () => {
      strictEqual(injector.provide(serialNoToken), 1111);
    });
  });

  describe('resolving dependencies', () => {
    it('should resolve dependencies in constructor', () => {
      class Engine {
        constructor(public car: Car, public serial: number) {
        }
      }

      injector.set({ token: Engine, factory: () => Engine });

      Injector.bindParameter(Engine, Car, 0);
      Injector.bindParameter(Engine, serialNoToken, 1);

      const engine = injector.provide(Engine);

      ok(engine.car instanceof Car);
      strictEqual(engine.serial, 1111);
    });

    it('should resolve dependencies being properties', () => {
      class Engine {
        car!: Car;
        serial!: number;
      }

      injector.set({ token: Engine, factory: () => Engine });

      Injector.bindProperty(Engine, Car, 'car');
      Injector.bindProperty(Engine, serialNoToken, 'serial');

      const engine = injector.provide(Engine);

      ok(engine.car instanceof Car);
      strictEqual(engine.serial, 1111);
    });
  });

  describe('resolving dependencies from different injector', () => {
    let enginesInjector: Injector;
    let trucksInjector: Injector;

    class Engine {}
    class Truck {
      constructor(public engine: Engine) {}
    }

    beforeEach(() => {
      enginesInjector = new Injector('Engines');
      trucksInjector = new Injector('Trucks');

      /**
       * Below code can be omitted when using decorators:
       * @Injectable(enginesInjector) class Engine {}
       * @Injectable(trucksInjector) class Truck {
       *   constructor(public engine: Engine) {}
       * }
       */

      enginesInjector.set({ token: Engine, factory: () => Engine });
      trucksInjector.set({ token: Truck, factory: () => Truck });
      Injector.bindParameter(Truck, Engine, 0);
    });

    it('should work when additional providers option is given', () => {
      const truck = trucksInjector.provide(Truck, [ enginesInjector ]);

      ok(truck instanceof Truck);
      ok(truck.engine instanceof Engine);
    });
    it('should throw when provider for foreign dependency is missing', () => {
      throws(() => {
        trucksInjector.provide(Truck);
      });
    });
  });

  describe('dependencies tracing', () => {
    it('should allow to get injection path up to the top of current service', () => {
      class Wheels {}
      class Suspension {
        wheels!: Wheels;
      }
      class Truck {
        suspension!: Suspension;
      }

      injector.set({ token: Wheels, factory: () => Wheels });
      injector.set({ token: Suspension, factory: () => Suspension });
      injector.set({ token: Truck, factory: () => Truck });

      Injector.bindProperty(Suspension, Wheels, 'wheels');
      Injector.bindProperty(Truck, Suspension, 'suspension');

      const truck = injector.provide(Truck);

      ok(truck instanceof Truck);
      ok(truck.suspension instanceof Suspension);
      ok(truck.suspension.wheels instanceof Wheels);
    });
  });

  describe('providers defined on Injector', () => {
    it('should be used when additional providers are given in provide method', () => {
      const models = new Injector('Models');
      const services = new Injector('Services');
      const repositories = new Injector('Repositories', [models]);
      const controllers = new Injector('Controllers', [services]);

      @Injectable(models) class Model {}
      @Injectable(services) class Service {}
      @Injectable(repositories) class Repository {
        constructor(public model: Model) {}
      }
      @Injectable(controllers) class Controller {
        constructor(public service: Service, public repository: Repository) {}
      }

      // Alternative version when not using decorators.

      // models.set({ token: Model, factory: () => Model });
      // services.set({ token: Service, factory: () => Service });
      // repositories.set({ token: Repository, factory: () => Repository });
      // controllers.set({ token: Controller, factory: () => Controller });

      // Injector.bindParameter(Repository, Model, 0);
      // Injector.bindParameter(Controller, Service, 0);
      // Injector.bindParameter(Controller, Repository, 1);

      const ctrl = controllers.provide(Controller, [repositories]);

      ok(ctrl.service instanceof Service);
      ok(ctrl.repository instanceof Repository);
      ok(ctrl.repository.model instanceof Model);
    });
  });
});

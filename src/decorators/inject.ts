import { InjectionKey } from '../injection-token';
import { Injector } from '../injector';

export function Inject<T>(
  token: InjectionKey<T>
): PropertyDecorator & ParameterDecorator {
  return (target: Object, propertyKey: string | symbol, parameterIndex?: number): void => {
    if (typeof parameterIndex !== 'undefined') {
      Injector.bindParameter(target, token, parameterIndex);
    } else if (typeof propertyKey !== 'undefined') {
      Injector.bindProperty(target.constructor, token, propertyKey);
    }
  };
}

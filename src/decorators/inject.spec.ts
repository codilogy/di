import { ok } from 'assert';
import { Injectable } from './injectable';
import { Inject } from './inject';
import { Container } from '../container';
import { InjectionToken } from '../injection-token';
import { InjectionType } from '../injection-type';

describe('Inject decorator', () => {
  describe('class provisioning', () => {
    it('should inject in constructor', () => {
      @Injectable()
      class Engine {}
      @Injectable()
      class Car {
        constructor(@Inject(Engine) public engine: Engine) {}
      }

      const car = Container.provide(Car);

      ok(car && car.engine instanceof Engine);
    });
    it('should inject as property', () => {
      @Injectable()
      class Engine {}
      @Injectable()
      class Car {
        @Inject(Engine)
        engine!: Engine;
      }

      const car = Container.provide(Car);

      ok(car && car.engine instanceof Engine);
    });
  });
  describe('with nested injector', () => {
    it('should inject dependencies from it', () => {
      const models = Container.create('Models');
      const repositories = Container.create('Repositories', [models]);

      @Injectable(models) class Model {}
      @Injectable(repositories) class Repository {
        constructor(@Inject(Model) public model: any) {}
      }

      const repo = repositories.provide(Repository);

      ok(repo.model instanceof Model);
    });
    it('should inject manually defined tokens with custom type', () => {
      const ModelsNamespace: symbol = Symbol('Models');
      const ModelsInjector = Container.for(ModelsNamespace);

      const ServicesNamespace: symbol = Symbol('Services');
      const ServicesInjector = Container.create(ServicesNamespace, [
        ModelsInjector
      ]);

      const Service = () => Injectable(ServicesInjector);
      const Model = () => {
        return (target: any) => {
          ModelsInjector.set({
            type: InjectionType.Value,
            token: target,
            factory: () => target
          });
        }
      };

      @Model()
      class MyModel {}

      @Service()
      class MyService {
        constructor(@Inject(MyModel) public model: typeof MyModel) {}
      }

      const service = ServicesInjector.provide(MyService);

      ok(service.model === MyModel);
      ok(service.model instanceof MyModel === false);
    });
  });
  describe('custom token', () => {
    it('should inject using custom token as string', () => {
      @Injectable('MyEngine')
      class Engine {}
      @Injectable()
      class Car {
        constructor(@Inject('MyEngine') public engine: Engine) {}
      }

      const car = Container.provide(Car);

      ok(car && car.engine instanceof Engine);
    });
    it('should inject using custom token as Symbol', () => {
      const MyEngine = Symbol('MyEngine')

      @Injectable(MyEngine)
      class Engine {}
      @Injectable()
      class Car {
        constructor(@Inject(MyEngine) public engine: Engine) {}
      }

      const car = Container.provide(Car);

      ok(car && car.engine instanceof Engine);
    });
    it('should inject using custom token as InjectionToken', () => {
      const MyEngine = new InjectionToken('MyEngine');

      @Injectable(MyEngine)
      class Engine {}
      @Injectable()
      class Car {
        constructor(@Inject(MyEngine) public engine: Engine) {}
      }

      const car = Container.provide(Car);

      ok(car && car.engine instanceof Engine);
    });
  });
  describe('custom namespace/injector', () => {
    it('should inject parameter from custom namespace [#1]', () => {
      const namespace = Container.for('Engines');

      @Injectable(namespace)
      class Engine {}
      @Injectable()
      class Car {
        constructor(@Inject(Engine) public engine: Engine) {}
      }

      const car = Container.provide(Car, [namespace]);

      ok(car && car.engine instanceof Engine);
    });
    it('should inject property from custom namespace [#1]', () => {
      const namespace = Container.for('Engines');
      const cars = Container.for('Cars');

      @Injectable(namespace)
      class Engine {}

      @Injectable(cars)
      class Car {
        @Inject(Engine)
        engine?: Engine;
      }

      const car = cars.provide(Car, [namespace]);

      ok(car && car.engine instanceof Engine);
    });
  });
});

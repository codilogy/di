import { ok, doesNotThrow } from 'assert';
import { Injectable } from './injectable';
import { Container } from '../container';
import { InjectionToken } from '../injection-token';
import { Inject } from './inject';

describe('Injectable decorator', () => {
  describe('custom namespace/injector', () => {
    it('should set class as a dependency in default namespace', () => {
      @Injectable()
      class Service {}

      ok(Container.provide(Service) instanceof Service);
    });

    it('should allow to set class as a dependency in custom namespace', () => {
      const controllersNamespace = Symbol('ControllersInjector');
      const controllersInjector = Container.for(controllersNamespace);

      @Injectable(controllersInjector)
      class Service {}

      ok(controllersInjector.provide(Service) instanceof Service);
    });

    it('should not throw during class decoration when service is from different injector [#3]', () => {
      ok(typeof Reflect.getMetadata === 'function');

      const controllersNamespace = Symbol('ControllersInjector');
      const controllersInjector = Container.for(controllersNamespace);

      @Injectable() // Injected by GlobalInjectorNamespace
      class SomeService {}

      doesNotThrow(() => {
        @Injectable(controllersInjector)
        class SomeController {
          constructor(public service: SomeService) {}
        }
      });

      doesNotThrow(() => {
        @Injectable(controllersInjector)
        class SomeController {
          constructor(@Inject(SomeService) public service: SomeService) {}
        }
      });
    });
  });
  describe('custom token', () => {
    it('should allow for custom token as string', () => {
      @Injectable('CustomTokenString')
      class Service {}

      ok(Container.provide('CustomTokenString') instanceof Service);
    });

    it('should allow for custom token as Symbol', () => {
      const customToken = Symbol('CustomTokenSymbol')

      @Injectable(customToken)
      class Service {}

      ok(Container.provide(customToken) instanceof Service);
    });

    it('should allow for custom token as InjectionToken', () => {
      const customToken = new InjectionToken<Service>('CustomTokenInjectionToken');

      @Injectable(customToken)
      class Service {}

      ok(Container.provide(customToken) instanceof Service);
    });

    it('should allow token with namespace', () => {
      @Injectable('CustomToken', Container.for('ControllersInjector'))
      class Service {}

      ok(Container.for('ControllersInjector').provide('CustomToken') instanceof Service);
    });
  });
});

import 'reflect-metadata';

import { InjectionFactory, InjectionKey, InjectionToken } from '../injection-token';
import { GlobalInjector } from '../global-injector';
import { Injector } from '../injector';
import { InjectionType } from '../injection-type';

/**
 * @param injector Optional `Injector` instance.
 */
export function Injectable(injector?: Injector): ClassDecorator;

/**
 * @typeparam T Generic type parameter.
 * @param token `InjectionToken` instance or `string` for token name.
 * @param injector Optional `Injector` instance.
 */
export function Injectable<T>(token?: InjectionKey<T>, injector?: Injector): ClassDecorator;

/**
 * Annotates class as injectable service. It does not require to provide token,
 * however you can do so. A token can be:
 * - `InjectionToken` instance
 * - `string`
 * - `symbol`
 *
 * If token is not provided, it will be created form decorated class.
 *
 * Decorator also accepts `Injector` instance as a second argument (or as a first one
 * when token is omitted). This allows to register service in specific Injector (namespace).
 *
 * Usage:
 * ```typescript
 * @Injectable()
 * class Car {}
 * ```
 *
 * Using `Injectable` decorator is equivalent to:
 * ```typescript
 * class Car {}
 *
 * Container.set({
 *   type: InjectionType.Class,
 *   token: new InjectionToken('Car'),
 *   factory() {
 *     return Car;
 *   }
 * });
 * ```
 *
 * This decorator can be used only with classes, so there is no option to change the `type` of
 * the injection. It will be always `InjectionType.Class`.
 */
export function Injectable<T>(
  tokenOrInjector?: Injector | InjectionKey<T>,
  injector: Injector = GlobalInjector
): ClassDecorator {
  return (target: any): void => {
    let token: InjectionKey<T> | undefined = void 0;

    if (typeof tokenOrInjector !== 'undefined' && tokenOrInjector instanceof Injector) {
      injector = tokenOrInjector;
    }

    if (!tokenOrInjector || tokenOrInjector instanceof Injector) {
      token = target;
    }

    if (typeof tokenOrInjector === 'string' || typeof tokenOrInjector === 'symbol' || tokenOrInjector instanceof InjectionToken) {
      token = tokenOrInjector;
    }

    if (typeof token === 'undefined') {
      throw new Error('Cannot resolve token value');
    }

    // Use `emitDecoratorMetadata` to auto-inject class based constructor parameters.
    if (Reflect && typeof Reflect.getMetadata === 'function') {
      designParameters(target).forEach((value: any, parameterIndex: number) => {
        if (typeof value === 'function') {
          Injector.bindParameter(target, value, parameterIndex);
        }
      });
    }

    injector.set<T>({
      token,
      type: InjectionType.Class,
      factory: tokenValueFactory<T>(target)
    });
  };
}

const tokenValueFactory = <T>(value: T): InjectionFactory<T> => {
  return () => value;
};

const designParameters = (target: any): any[] => {
  const paramtypes = Reflect.getMetadata('design:paramtypes', target);

  return Array.isArray(paramtypes) ? paramtypes : [];
};

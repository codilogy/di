export interface Indexable<V extends any = any> {
  [key: string]: V
}

export interface Constructor<T> extends Indexable {
  new (...args: any[]): T;
}

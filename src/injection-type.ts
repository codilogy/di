/**
 * Dependency can be on of the 3 types. Type is important
 * when creating an instance of the factory function.
 */
export enum InjectionType {
  Class, Factory, Value
}

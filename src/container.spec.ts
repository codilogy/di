import { ok, throws } from 'assert';
import { Container } from './container';
import { Injectable } from './decorators/injectable';

describe('Container', () => {
  describe('namespace', () => {
    it('should have default namespace', () => {
      @Injectable()
      class Service {}

      ok(Container.provide(Service) instanceof Service);
    });
    it('should allow to setup custom namespace using string', () => {
      const namespace: string = 'namespace';
      const injector = Container.for(namespace);

      @Injectable(injector)
      class Service {}

      ok(injector.provide(Service) instanceof Service);
      throws(() => {
        Container.provide(Service);
      });
    });
    it('should allow to setup custom namespace using symbol', () => {
      const namespace: symbol = Symbol('namespace');
      const injector = Container.for(namespace);

      @Injectable(injector)
      class Service {}

      ok(injector.provide(Service) instanceof Service);
      throws(() => {
        Container.provide(Service);
      });
    });
  });

  describe('mixed namesapces', () => {
    it('should provide services from global namespace in service from custom namespace', () => {
      const Controllers = Container.for('ControllersContainer');

      @Injectable()
      class Database {}

      @Injectable(Controllers)
      class Controller {
        constructor(public db: Database) {}
      }

      const ctrl = Controllers.provide(Controller);

      ok(ctrl.db instanceof Database);
    });
  });
});

import { Injector, InjectorNamespace } from './injector';

export const GlobalInjectorNamespace: InjectorNamespace = Symbol('GlobalInjectorNamespace');

export const GlobalInjector: Injector = new Injector(GlobalInjectorNamespace, []);

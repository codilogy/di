import { InjectionToken, InjectionKey, InjectionFactory } from './injection-token';
import { Injector } from './injector';
import { InjectionType } from './injection-type';

export interface InjectionConfig<T> {
  token: InjectionKey<T>;
  factory: InjectionFactory<T>;
  type?: InjectionType;
  providers?: Injector[];
}

export interface InjectionMetadata<T> extends InjectionConfig<T> {
  token: InjectionToken<T>;
  type: InjectionType;
}

export interface InjectionPointOptions<T> {
  token: InjectionKey<T>;
  parameterIndex?: number;
  propertyKey?: string | symbol;
}

export interface InjectionPointMetadata<T> extends InjectionConfig<T> {
  token: InjectionKey<T>;
  parameterIndex?: number;
  propertyKey?: string | symbol;
}

/**
 * Injection metadata keeps information about injectable,
 * that allows to find it and initialize.
 */
export class InjectionMetadata<T> {
  constructor(
    public token: InjectionToken<T>,
    public factory: InjectionFactory<T>,
    public type: InjectionType = InjectionType.Class
  ) {}

  get [Symbol.toStringTag]() {
    return `InjectionMetadata<${String(this.token)}>`;
  }
}

/**
 * Injection metadata keeps information about injectable,
 * that allows to find it and initialize.
 */
export class InjectionPointMetadata<T> {
  constructor(options: InjectionPointOptions<T>) {
    Object.assign(this, options);
  }

  get [Symbol.toStringTag]() {
    return `InjectionPointMetadata<${String(this.token)}>`;
  }
}

import { InjectionConfig, InjectionMetadata, InjectionPointMetadata } from './injection-metadata';
import { InjectionKey, InjectionToken } from './injection-token';
import { ParametersMetadata } from './metadata/parameters';
import { PropertiesMetadata } from './metadata/properties';
import { Constructor } from './common/types';
import * as debugFactory from 'debug';
import { InjectionType } from './injection-type';

const debug = debugFactory('di:injector');

export type InjectorNamespace = string | symbol;

/**
 * Injector keeps && initializes services.
 * This is low-level object.
 */
export class Injector {
  static bindParameter<T>(target: object, token: InjectionKey<T>, parameterIndex: number): void {
    /**
     * Check if parameter is already decorated. Used to avoid race condition when parameter
     * can be decorated once by `ParameterDecorator` and then by `ClassDecorator` when
     * `emitDecoratorMetadata` is set to `true`. Using the fact parameters are decorated before
     * class itself, we give `@Inject` preference before `@Injectable`.
     */
    const isBound = ParametersMetadata.search(
      target,
      (item: InjectionPointMetadata<T>): boolean => {
        return item.parameterIndex === parameterIndex;
      }
    );

    if (!isBound) {
      ParametersMetadata.add(target,
        new InjectionPointMetadata({
          token,
          parameterIndex
        }));
    }
  }

  static bindProperty<T>(target: object, token: InjectionKey<T>, propertyKey: string | symbol): void {
    PropertiesMetadata.add(target,
      new InjectionPointMetadata({
        token,
        propertyKey
      }));
  }

  private static createMetadata<T>(config: InjectionConfig<T>): InjectionMetadata<T> {
    const token = InjectionToken.create(config.token);
    const metadata = config instanceof InjectionMetadata ? config : new InjectionMetadata(token, config.factory, config.type);

    const target = config.factory();
    if (typeof target === 'function') {
      token.link(target);
    }

    return metadata;
  }

  /**
   * Mapping between InjectionToken and InjectionMetadata.
   */
  private readonly metadata = new Map<InjectionToken<any>, InjectionMetadata<any>>();

  /**
   * Mapping between InjectionMetadata and created instance.
   */
  private readonly instance = new WeakMap<InjectionMetadata<any>, any>();

  constructor(
    private namespace: InjectorNamespace,
    private providers: Injector[] = []
  ) {}

  get [Symbol.toStringTag]() {
    return `Injector(${String(this.namespace)})`;
  }

  tokens() {
    return this.metadata.keys();
  }

  has<T>(token: InjectionToken<T>): boolean {
    return this.metadata.has(token);
  }

  set<T>(config: InjectionConfig<T>): InjectionToken<T> {
    const metadata = Injector.createMetadata<T>(config);
    const token = metadata.token;

    if (this.has(token)) {
      // TODO: Implement "multiple"
      console.warn(
        `Token "${token}" metadata is already registered. If you intended to add multiple values then set "multiple: true" option on each registration.`
      );
    } else {
      this.metadata.set(token, metadata);
    }

    return token;
  }

  /**
   * Exchanges injection key for injection metadata.
   * @param target
   */
  get<T>(target: InjectionKey<T>): InjectionMetadata<T> | undefined {
    const token = InjectionToken.resolve(target);

    if (!token) {
      return void 0;
    }

    if (!(token instanceof InjectionToken)) {
      throw new TypeError(`Token for "${String(target)}" is invalid`);
    }

    return this.metadata.get(token);
  }

  provide<T>(target: InjectionKey<T>, providers: Injector[] = []): T {
    for (const injector of this.resolveProviders(providers)) {
      const metadata = injector.get(target);
      if (metadata) {
        debug(`${this}.provide(%s)`, metadata.token);
        return injector.resolve(metadata, providers);
      }
    }

    throw new Error(`Cannot provide ${String(target)}.`);
  }

  /**
   * Return list of providers to lookup for the service. By default injector itself is included together with
   * GlobalInjectorNamespace (see Container) if `this` is not already a GlobalInjectorNamespace (uniques is delivered by Set object
   * capabilities).
   */
  private resolveProviders(providers: Injector[] = []): Set<Injector> {
    return providers.reduce((injectors: Set<Injector>, injector: Injector) => {
      return injectors.add(injector);
    }, new Set([this, ...this.providers]));
  }

  /**
   * Returns instance of a service from instances cache or by creating and caching new one.
   * @param metadata
   * @param providers
   */
  private resolve<T>(metadata: InjectionMetadata<T>, providers: Injector[] = []): T {
    if (this.instance.has(metadata)) {
      return this.instance.get(metadata);
    }

    const wrongValueTypeError = () => {
      throw new Error(`${metadata.token} was provided as a "${InjectionType[metadata.type]}" type, but it seems`
        + ` it cannot be instantiated because of wrong type. Expected "function" but got "${typeof value}".`);
    };

    const value = metadata.factory();

    let injectable;

    switch (metadata.type) {
      case InjectionType.Value:
        injectable = value;
        break;

      case InjectionType.Factory:
        if (typeof value === 'function') {
          injectable = (value as Function)(...providers);
        } else {
          wrongValueTypeError();
        }
        break;

      case InjectionType.Class:
      default:
        if (typeof value === 'function') {
          injectable = this.construct(value, providers);
        } else {
          wrongValueTypeError();
        }
        break;
    }

    if (injectable) {
      this.instance.set(metadata, injectable);
      return injectable;
    }

    throw new Error('Cannot provide ' + metadata.token + ' from ' + this);
  }

  private construct<T>(target: Constructor<T> | Function, providers: Injector[] = []): T | undefined {
    const parameters = this.resolveParameters(target);
    const properties = this.resolveProperties(target);
    const instance = Reflect.construct(
      target,
      parameters.map((parameter) => {
        return this.resolveDependency(parameter, providers);
      })
    );

    if (properties) {
      properties.forEach((property, key) => {
        const descriptor = Reflect.getOwnPropertyDescriptor(instance, key);
        Reflect.defineProperty(instance, key, {
          ...descriptor,
          value: this.resolveDependency(property, providers)
        });
      });
    }

    return instance;
  }

  private resolveDependency<T>(target: InjectionPointMetadata<T>, providers: Injector[]): T | undefined {
    const errors: Error[] = [];
    for (const injector of [this, ...providers]) {
      try {
        const service = injector.provide(target.token);

        if (service) {
          return service;
        }
      } catch (error) {
        errors.push(error);
      }
    }

    if (errors.length) {
      throw new Error(errors.join('\n'));
    }
  }

  private resolveParameters(value: any): InjectionPointMetadataSet {
    return Array.from(ParametersMetadata.get(value)).reduce(
      (dependencies, injectedParameter) => {
        const { parameterIndex } = injectedParameter;
        if (typeof parameterIndex === 'number') {
          dependencies[parameterIndex] = injectedParameter;
        }
        return dependencies;
      },
      [] as InjectionPointMetadataSet
    );
  }

  private resolveProperties(value: any): InjectionPointMetadataMap {
    return Array.from(PropertiesMetadata.get(value)).reduce((propertiesMap, injectedProperty) => {
      const { propertyKey } = injectedProperty;

      if (typeof propertyKey === 'string' || typeof propertyKey === 'symbol') {
        propertiesMap.set(propertyKey, injectedProperty);
      }

      return propertiesMap;
    }, new Map());
  }
}

type InjectionPointMetadataSet = Array<InjectionPointMetadata<any>>;

type InjectionPointMetadataMap = Map<string | symbol, InjectionPointMetadata<any>>;

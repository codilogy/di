import { InjectionConfig, InjectionMetadata } from './injection-metadata';
import { InjectionKey, InjectionToken } from './injection-token';
import { Injector, InjectorNamespace } from './injector';
import { GlobalInjector, GlobalInjectorNamespace } from './global-injector';

const ContainerRegistry = new Map<InjectorNamespace, Injector>();

/**
 * Container is a global object that holds Injectors.
 *
 * @global
 */
export class Container {
  /**
   * Get existing or create new `Injector`. This injector is stored
   * internally so it can be obtained from any place through `Container`.
   * Each created `Injector` has injected `GlobalInjector` as provider.
   * If you need clean and detached `Injector` instance use `Container.create`.
   * @param namespace
   */
  static for(namespace: InjectorNamespace): Injector {
    if (namespace === GlobalInjectorNamespace) {
      return GlobalInjector;
    }

    if (ContainerRegistry.has(namespace)) {
      return ContainerRegistry.get(namespace) as Injector;
    } else {
      const injector = this.create(namespace, [GlobalInjector]);
      ContainerRegistry.set(namespace, injector);
      return injector
    }
  }

  static set<T>(injectableMetadata: InjectionConfig<T>): InjectionToken<T> {
    return this.for(GlobalInjectorNamespace).set(injectableMetadata);
  }

  static get<T>(target: InjectionKey<T>): InjectionMetadata<T> | undefined {
    return this.for(GlobalInjectorNamespace).get(target);
  }

  static provide<T extends any>(target: InjectionKey<T>, providers: Injector[] = []): T | undefined {
    return this.for(GlobalInjectorNamespace).provide(target, providers);
  }

  /**
   * Creates empty and detached `Injector`. This is convenient when you need
   * a short living `Injector` instance, which shouldn't be stored in containers
   * registry (it will be not accessible through `Container.for`).
   * Also it doesn't have access to values provided by `GlobalInjector`.
   *
   * @param namespace
   * @param injectors
   */
  static create(namespace: InjectorNamespace, injectors: Injector[] = []): Injector {
    return new Injector(namespace, injectors);
  }

  constructor() {
    throw new Error('Container cannot be instantiated. Use `Container.for` if you need separate injection containers.');
  }
}

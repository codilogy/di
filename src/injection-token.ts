import { Constructor } from "./common/types";

/**
 * Virtual representation of injectable object. Because injectables
 * can be of different type, InjectionToken serves as a universal
 * representation of any injectable object.
 */
export class InjectionToken<T> {
  /**
   * Keeps references between Class objects and created InjectionTokens
   * to allow reverse lookup of token by Class object.
   */
  static readonly classMap = new WeakMap<any, InjectionToken<any>>();

  /**
   * Keeps references between names and created InjectionTokens
   * to allow reverse lookup of token by its name.
   */
  static readonly namesMap = new Map<string | symbol, InjectionToken<any>>();

  /**
   * Creates InjectionToken instance from any acceptable value,
   * that is string or Class. When InjectionToken is given it is
   * passed by. When token cannot be created, error is thrown.
   * @param injectionKey
   */
  static create<T>(injectionKey: InjectionKey<T>): InjectionToken<T> {
    if (typeof injectionKey === 'string' || typeof injectionKey === 'symbol') {
      return new InjectionToken<T>(injectionKey);
    } else if (typeof injectionKey === 'function') {
      return new InjectionToken<T>(injectionKey.name);
    } else if (injectionKey instanceof InjectionToken) {
      return injectionKey;
    } else {
      throw new Error(`InjectionToken for "${String(injectionKey)}" cannot be created.`);
    }
  }

  /**
   * Resolves given value to InjectionToken, by looking up in internal
   * map of already created tokens.
   * @param injectionKey
   */
  static resolve<T>(injectionKey: InjectionKey<T>): InjectionToken<T> | undefined {
    if (injectionKey instanceof InjectionToken) {
      return injectionKey;
    } else if (typeof injectionKey === 'string' || typeof injectionKey === 'symbol') {
      return InjectionToken.namesMap.get(injectionKey);
    } else if (typeof injectionKey === 'function') {
      return InjectionToken.namesMap.get(injectionKey.name);
    } else {
      return InjectionToken.classMap.get(injectionKey);
    }
  }

  constructor(private readonly name: string | symbol) {
    InjectionToken.namesMap.set(name, this);
  }

  get [Symbol.toStringTag]() {
    return `InjectionToken<${String(this.name)}>`;
  }

  /**
   * Method for Injector which should link token created by itself
   * with class constructor if this is the case.
   * TODO: Investigate possibility to move this logic from Injector to InjectionToken.constructor
   * @param target
   */
  link(target: Constructor<T> | T): void {
    InjectionToken.classMap.set(target, this);
  }
}

/**
 * Represents object which can be used as service identity. By default identity
 * is resolved with InjectionToken. If you use class constructor
 * or string as InjectionKey, then they will be converted to InjectionToken.
 */
export type InjectionKey<T> = Constructor<T> | InjectionToken<T> | string | symbol;

export interface InjectionFactory<T> {
  (...args: any[]): Constructor<T> | T;
}
